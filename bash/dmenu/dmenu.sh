#! /usr/bin/bash

# 1) Gather lines
# 2) Format lines
# 3) Present dmenu
# 4) format selection
# 5) run selection

manual="$(man -k . | awk '{print $1}' | dmenu -p Manual:)"

if [[ ! -z "$manual" ]]; then
  st -T "Manual" -e man $manual
fi

exit 0

