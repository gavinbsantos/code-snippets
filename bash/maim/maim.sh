#! /bin/fish

set filename $(echo | dmenu -p "maim -s File name: ")

if test -f $filename; then
  # ask if you want to overwrite
  # if $(echo "yes\nno" | dmenu -p "Do you want to overwrite?") == "yes"; then 
else
  maim -s "$HOME/Projects/code_snippets/bash/maim/$filename"
end
