#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Thing
{
    char * myString;

} Thing;

void freeThing(Thing * thing)
{
    free(thing->myString);
    free(thing);
}

int main ()
{
    Thing * test = (Thing *) malloc(sizeof(Thing));
    printf("Enter string: ");
    char temp[256];
    scanf(" %[^\n]s", temp);

    test->myString = (char *) malloc(strlen(temp) + 1);
    strcpy(test->myString, temp);
    printf("%s\n", test->myString);

    freeThing(test);

    return 0;
}
