#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
    char * myString;
} Thing;

int main ()
{
    char * test1;
    test1 = "hello there";
    printf("%s\n", test1);

    Thing test2;
    printf("Enter string: ");
    char temp2[256];
    scanf("%[^\n]s", temp2);
    test2.myString = (char *) malloc(strlen(temp2) + 1);
    printf("%s\n", test2.myString);
    free(test2.myString);

    Thing * test3 = (Thing *) malloc(sizeof(Thing));
    printf("Enter String: ");
    char temp3[256];
    scanf(" %[^\n]s", temp3);
    test3->myString = (char *) malloc(strlen(temp3) + 1);
    strcpy(test3->myString, temp3);
    printf("%s\n", test3->myString);
    free(test3->myString);
    free(test3);


    return 0;
}
