#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Thing
{
    int number;
    int number2;
} Thing;

typedef struct
{
    Thing * array;
    unsigned int length;
} ThingArray;

ThingArray array;

void addItem(int number, int number2)
{
    if (array.length == 0)
    {
        array.array = (Thing *) malloc(sizeof(Thing));
        array.array[0].number = number;
        array.array[0].number2 = number2;
        array.length++;
    } else {
        // array.length is not 0
        array.array = realloc(array.array, (array.length + 1) * sizeof(Thing));
        if (array.array)
        {
            array.array[array.length].number = number;
            array.array[array.length].number2 = number2;
            array.length++;
        }
    }
}

void removeItem (int index)
{
    ThingArray newArray;
    newArray.length = array.length - 1;
    newArray.array = (Thing *) malloc(sizeof(Thing) * newArray.length);
    memmove(newArray.array, array.array, sizeof(Thing) * index);
    memmove(&newArray.array[index], &array.array[index + 1], sizeof(Thing) * (array.length - index - 1));

    free(array.array);
    array.array = newArray.array;
    array.length = newArray.length;
}

void freeArray (ThingArray * arr)
{
    while (arr->length > 0)
    {
        removeItem(0);
    }
}

void printArray (ThingArray arr)
{
    for (int i = 0; i < arr.length; i++)
    {
        printf("index: %d, num: %d, num2: %d\n", i, arr.array[i].number, arr.array[i].number2);
    }

    printf("length: %d\n\n", arr.length);
}

int main ()
{
    array.array = 0;
    array.length = 0;


    addItem(1, 45);
    printArray(array);
    addItem(2, 9);
    printArray(array);
    addItem(4, 8);
    printArray(array);
    addItem(3, 5);
    printArray(array);
    addItem(69, 22);
    printArray(array);

    printArray(array);

    printf("\nNow to remove items from the array\n");

    printf("Removing index 2\n");
    removeItem(2);
    printArray(array);
    printf("Removing index 0\n");
    removeItem(0);
    printArray(array);




    freeArray(&array);

    return 0;
}
