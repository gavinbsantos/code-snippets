#include <stdio.h>
#include <string.h>

int main()
{
    // char buffer[30];
    int buffer;
    FILE * write_ptr;
    FILE * read_ptr;
    write_ptr = fopen("file.bin", "wb");

    /* buffer[0] = '\0'; */

    /* while (buffer[0] == '\0') */
    /* { */
    /*     printf("Enter a string: "); */
    /*     scanf("%s", &buffer); */
    /*     if (strlen(buffer) >= 30) */
    /*     { */
    /*         printf("That is not valid\n"); */
    /*         buffer[0] = '\0'; */
    /*     } */
    /* } */
    buffer = 12;
    fwrite(&buffer, sizeof(int), 1, write_ptr);
    fclose(write_ptr);

    read_ptr = fopen("file.bin", "rb");
    fscanf(read_ptr, "%i", &buffer);
    printf("%i", buffer);
    fclose(read_ptr);

    return 0;
}
