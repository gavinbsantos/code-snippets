#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int someFunction(const char * string)
{
    return strlen(string) + 1;
}

int main()
{
    FILE * read_ptr = fopen("test1.map", "r");
    char line[256];
    line[255] = '\0';

    if (read_ptr)
    {
        printf("\"Test string\": %i\n", someFunction("Test string"));
        while (fscanf(read_ptr, "%256[^\n]\n", line) == 1)
        {
            printf("line: %s, someFunction(): %d\n", line, someFunction(line));
        }

        fclose(read_ptr);
    }

    return 0;
}

