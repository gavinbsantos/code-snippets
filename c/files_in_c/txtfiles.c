#include <stdio.h>
#include <string.h>

int main()
{
    char buffer[100] = "hello there,hiThere,greetings\n";
    FILE * write_ptr;
    FILE * read_ptr;

    write_ptr = fopen("file.txt", "w");
    fwrite(&buffer, sizeof(char), strlen(buffer), write_ptr);
    strcpy(buffer, "testing,hello,thing\n");
    fwrite(&buffer, sizeof(char), strlen(buffer), write_ptr);
    fclose(write_ptr);

    strcpy(buffer, "thingy");
    printf("%s\n", buffer);

    read_ptr = fopen("file.txt", "r");
    char buff1[25], buff2[25], buff3[25];
    for (int i = 0; i < 3; i++)
    {
        fscanf(read_ptr, "%[^,],%[^,],%[^\n]\n", buff1, buff2, buff3);
        printf("\nname: %s\n", buff1);
        printf("star: %s\n", buff2);
        printf("favorite item: %s\n", buff3);
    }

    printf("\n--- Now for the while loop ---\n");

    fseek(read_ptr, 0, SEEK_SET);

    while (!feof(read_ptr))
    {
        fscanf(read_ptr, "%[^,],%[^,],%[^\n]\n", buff1, buff2, buff3);
        printf("\nname: %s\n", buff1);
        printf("star: %s\n", buff2);
        printf("favorite item: %s\n", buff3);
    }


    printf("\n--- Now we read the file line by line ---\n");

    fseek(read_ptr, 0, SEEK_SET);

    printf("\nbuffer at start: %s\n", buffer);
    while (!feof(read_ptr))
    {
        fscanf(read_ptr, "%[^\n]\n", buffer);
        printf("buffer: \"%s\"\n", buffer);
    }

    fclose(read_ptr);

    return 0;
}
