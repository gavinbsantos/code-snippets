#include <stdio.h>

#define thingy(x) _Generic( (x),        \
                            int: x++,    \
                            default: 33 \
                          )


int main ()
{
  printf("hello\n");
  printf("thingy value of 3: %d\n", thingy(3));
  printf("thingy value of '3': %d\n", thingy('a'));
  printf("thingy value of \"3\": %d\n", thingy("3"));
}
