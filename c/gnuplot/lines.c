#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char * argv[])
{
    FILE * commands = fopen("lines.txt", "w");
    FILE * dataFile = fopen("lines.dat", "w");
    FILE * gnuplot = popen("gnuplot -p", "w");

    srand(time(NULL));

    if (dataFile)
    {
        // Generate and write data
        for (int i = 0; i < 5; i++)
        {
            fprintf(dataFile, "%.1f %d\n", i, rand() % 9);
            fprintf(dataFile, "%.1f %d\n\n\n", i + 0.5, rand() % 9);
        }
        fclose(dataFile);
    }

    if (commands)
    {
        // Write the file to plot the data

        // Styles
        /* fprintf(commands, "set style line 1 \\\n\ */
/* linecolor rgb '#ad6000' \\\n\ */
/* linetype 2 linewidth 2 \\\n\ */
/* pointtype 8 pointsize 1.5\n"); */

        fprintf(commands, "set style line 1 \\\n\
linecolor rgb '#0060ad' \\\n\
linetype 1 linewidth 2 \\\n\
pointtype 7 pointsize 1.5\n");

        // plot commands
        fprintf(commands, "\nplot [0:6] [0:10] \"lines.dat\" index 0 with \\\n\
linespoints linestyle 1 title \"map\", \\\n");

        for (int i = 1; i < 5; i++)
        {
            fprintf(commands, "\"\" index %d with linespoints linestyle 1 title \"\"", i);
            if (i != 4)
            {
                fprintf(commands, ", \\");
            }
            fprintf(commands, "\n");
        }


        fclose(commands);
    }

    // Open gnuplot and display the data
    fprintf(gnuplot, "load \"lines.txt\"\n\n");
    fflush(gnuplot);



    return 0;
}
