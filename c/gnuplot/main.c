#include <stdio.h>

int main (int argc, char * argv[])
{
    FILE * gnuplot = popen("gnuplot -p", "w");
    // fprintf(gnuplot, "set title \"Hello there\"");
    fprintf(gnuplot, "plot '-' w l\n");
    for (int i = 0; i < 10; i++)
    {
        fprintf(gnuplot, "%i %i\n", i, i + 12);
    }

    fprintf(gnuplot, "e\n");
    fflush(gnuplot);

    return 0;
}
