#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

int getNumber(int lower, int upper)
{
    int diff = 0;
    while (diff == 0)
    {
        printf("Enter a number between %d and %d: ", lower, upper);
        scanf("%i", &diff);
        while (getchar() != '\n');
        if (diff <= 0 || diff > upper || diff < lower)
        {
            printf("That is not valid\n");
            diff = 0;
        }
    }
    return diff;
}

bool getYN()
{
    char input = ' ';
    bool answer = false;
    while (input == ' ')
    {
        printf("Enter yes or no [y/n]: ");
        input = getchar();
        while (getchar() != '\n');
        if (input != 'y' && input != 'n' && input != 'Y' && input != 'N')
        {
            printf("That is not valid\n");
            input = ' ';
        }
        switch (input)
        {
            case 'y':
            case 'Y':
                answer = true;
                break;
            case 'n':
            case 'N':
            default:
                answer = false;
        }
    }
    return answer;
}

int main()
{
    printf("Test 1: number\n");
    printf("diff: %i\n", getNumber(10, 20));
    printf("Test 2: y/n\n");
    if (getYN())
        printf("true\n");
    else
        printf("false\n");


    return 0;
}
