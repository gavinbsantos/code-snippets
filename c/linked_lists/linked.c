// gcc -Wall -g -o linked linked.c && valgrind -s --leak-check=full --show-leak-kinds=all --track-orgins=yes ./linked
#include <stdio.h>
#include <stdlib.h>

struct Node
{
  struct Node * next;
  int * data;
};

void add(struct Node * list, int data)
{
  struct Node * last = list;
  struct Node * new;

  // Find last item
  while (last->next != NULL) {last = last->next;}

  if (last->data == NULL)
    new = last;
  else
    new = (struct Node *) malloc(sizeof(struct Node));

  new->next = NULL;
  new->data = (int *) malloc(sizeof(int));
  *new->data = data;

  if (last != new)
    last->next = new;


}

void removeNode(struct Node * list, int index)
{
  struct Node * prev = list;
  int currIndex = 0;

  // Travel to previous node
  while (currIndex < index - 1 && prev->next != NULL) {prev = prev->next;}

  // Connect the two ends together
  // Free the saved node
}

void freeList(struct Node * list)
{
  struct Node * current = list;
  while (current != NULL)
  {
    struct Node * next = current->next;
    free(current->data);
    free(current);
    current = next;
  }
}

void printList(struct Node * list)
{
  int index = 0;
  struct Node * current = list;
  while (current != NULL)
  {
    printf("%d: ", index);
    printf("%d\n", *current->data);
    index++;
    current = current->next;
  }
}


int main(int argc, char * argv[])
{
  printf("--- linked lists ---\n\n");
  struct Node * head = (struct Node *) malloc(sizeof(struct Node));
  head->next = NULL;
  head->data = NULL;

  add(head, 12);
  add(head, 13);
  add(head, 11);
  add(head, 17);
  add(head, 17);
  add(head, 17);
  add(head, 17);
  add(head, 17);
  add(head, 17);
  add(head, 17);
  add(head, 17);
  printList(head);
  
  freeList(head);
  return 0;
}
