#define malloc(X) my_malloc(X, __FILE__, __LINE__, __FUNCTION__)

#include <stdio.h>
#include <stdlib.h>

struct MemNode
{
    void * p;
    size_t size;
    struct MemNode * nextNode;
};

static struct MemNode mem_list = 0;

void * my_malloc (size_t size, const char * file, int line, const char * func)
{
    void * p = malloc(size);
    printf("Allocated: %s, %i, %s, %p[%li]\n", file, line, func, p, size);

    // Linked list here
    // Add to the end of the list
    struct MemNode * nextNode = mem_list.nextNode;

    while (nextNode != 0)
    {
    }

    return p;
}

void my_free(void * p, const char * file, int line, const char * func)
{
    free(p);
    printf("Freed, %s, %i, %s, %p[%li]\n", file, line, func);
}
