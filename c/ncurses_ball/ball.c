#include <ncurses.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>



int main()
{
  srand(time(NULL));

  initscr();
  noecho();
  curs_set(FALSE);

  int max_y = 0, max_x = 0;
  getmaxyx(stdscr, max_y, max_x);
  int y = rand() % max_y, x = rand() % max_x;
  int next_y = 0, next_x = 0;
  int direction_y = rand() % 2;
  int direction_x = rand() % 2;
  int running = 1;
  /* int update = 1; */
  int delay = 100;
  char ch;

  timeout(delay);

  direction_y = !direction_y ? -1 : direction_y;
  direction_x = !direction_x ? -1 : direction_x;


  while (running)
  {
    // Input
    switch (ch = getch())
    {
      case 'q':
        /* fallthrough */
      case '\n':
        running = 0;
        break;

      case 'a':
        if (delay - 10 >= 10)
        {
          delay -= 10;
          timeout(delay);
        }
        break;

      case 'o':
        if (delay + 10 <= 1000)
        {
          delay += 10;
          timeout(delay);
        }
        break;


    }

    // Draw
    clear();
    mvprintw(0, 0, "delay: %d", delay);
    mvprintw(y, x, "o");
    refresh();

    getmaxyx(stdscr, max_y, max_x);

    next_x = x + direction_x;
    next_y = y + direction_y;

    if (next_y >= max_y || next_y < 0)
    {
      direction_y *= -1;
    }
    else
    {
      y += direction_y;
    }

    if (next_x >= max_x || next_x < 0)
    {
      direction_x *= -1;
    }
    else
    {
      x += direction_x;
    }
  }

  endwin();
  return 0;
}
