#include <ncurses.h>

// Compile with: gcc -lncurses init.c

int main() {
    initscr();

    printw("Hello World!\n");
    int height = 0;
    int width = 0;
    getmaxyx(stdscr, height, width);
    printw("The size of this window is %i by %i", width, height);
    refresh();

    getch();
    endwin();

    return 0;
}
