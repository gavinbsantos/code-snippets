#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_BUFFER_SIZE 32

int maxCol;
int maxRow;

int getNumber()
{
  // Fun fact: You can use getnstr() to not only get a string but also to 
  // enforce a maximum number of chars.
  char buffer[MAX_BUFFER_SIZE];
  buffer[0] = '\0';
  int numChars = 0;
  int number;

  curs_set(TRUE);

  while (true)
  {
    char input = getch();
    int exit = 0;

    switch (input)
    {
      // Enter
      case '\n':
        // check if buffer is a number
        number = atoi(buffer);

        if (number <= 0)
        {
          // Clear buffer on screen
          // We don't know where on the screen where the buffer is
          // This will not remove any other text.
          for (numChars; numChars > 0; numChars--)
          {
            printw("\b");
            delch();
          }

          buffer[0] = '\0';
          numChars = 0;
        }
        else
        {
          exit = 1;
        }
        break;

      // Backspace
      case '\x7f':
      case '\b':
        if (numChars > 0)
        {
          numChars--;
          buffer[numChars] = '\0';
          printw("\b");
          delch();
        }
        break;
      default:
        // Check if the new character will go over the buffer size or off the screen.
        if (numChars < MAX_BUFFER_SIZE && numChars + 1 < maxCol)
        {
          buffer[numChars] = input;
          numChars++;
          printw("%c", input);
        }
    }
    refresh();
    if (exit) { break; }
  }

  curs_set(FALSE);
  return number;
}

int main()
{
  initscr();
  noecho();
  cbreak();
  curs_set(FALSE);

  getmaxyx(stdscr, maxRow, maxCol);

  if (maxRow < 5 || maxCol < 50)
  {
    endwin();
    printf("Your screen size is too small.\n");
    return 0;
  }

  clear();
  mvprintw(0, 0, "Enter a positive number greater than 1: ");
  int number = getNumber();
  clear();

  printw("You typed %d", number);

  getch();
  endwin();
  return 0;
}
