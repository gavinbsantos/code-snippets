#include <ncurses.h>
#include <stdio.h>

int menu()
{
  clear();
  mvprintw(0, 0, "  --- Menu --- \n");
  printw("\n 1) option 1\n");
  printw(" 2) option 2\n");
  printw(" 3) option 3\n");
  printw(" 4) option 4\n");
  printw("\nPress a number: ");
  refresh();

  while (true)
  {
    switch (getch())
    {
      case '1':
        return 1;
      case '2':
        return 2;
      case '3':
        return 3;
      case '4':
        return 4;
    }
  }

  return -1;
}

int main()
{
  initscr();
  noecho();
  cbreak();
  curs_set(FALSE);

  int maxRow = 0, maxCol = 0;

  getmaxyx(stdscr, maxRow, maxCol);

  // Check the size of the window
  if (maxCol < 50 || maxRow < 10)
  {
    endwin();
    printf("The terminal size is too small.\n");
    return 0;
  }

  clear();
  int choice = menu();
  clear();
  mvprintw(0, 0, "You selected %d", choice);

  getch();
  endwin();
  return 0;
}
