#include <ncurses.h>

int main()
{
  // Init
  initscr();
  cbreak();
  noecho();
  curs_set(FALSE);

  // Program
  WINDOW * win = newwin(10, 10, 10, 10);

  mvwprintw(win, 1, 1, "Hello pe");
  mvwprintw(win, 2, 1, "is");
  box(win, 0, 0);

  refresh();
  wrefresh(win);
  getch();
  delwin(win);


  // Endit
  clear();
  printw("The End!");
  getch();
  endwin();
  return 0;
}
