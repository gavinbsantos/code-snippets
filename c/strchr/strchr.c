#include <stdio.h>
#include <string.h>

int validateEmail(char * email)
{
    char * atSymbol = strchr(email, '@');
    char * dotSymbol = strrchr(email, '.');

    if (atSymbol >= dotSymbol || atSymbol == 0 || dotSymbol == 0)
    {
        return 1;
    }
    return 0;
}

int main()
{
    char validEmail[] = "hello@email.com";
    char invalidEmail[] = "Hellothere.com";
    char invalidEmail2[] = "hello@hicomk";
    char invalidEmail3[] = "12";

    printf("Error Code for %s: %i\n", validEmail, validateEmail(validEmail));
    printf("Error Code for %s: %i\n", invalidEmail, validateEmail(invalidEmail));
    printf("Error Code for %s: %i\n", invalidEmail2, validateEmail(invalidEmail2));
    printf("Error Code for %s: %i\n", invalidEmail3, validateEmail(invalidEmail3));


    return 0;
}
