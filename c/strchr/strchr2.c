#include <stdio.h>
#include <string.h>


int main()
{
  char * tokens[5];
  memset(tokens, 0, sizeof(tokens));
  char buffer[] = "echo hello there";
  int index = 1;

  tokens[0] = buffer;
  for (int i = 0; i < sizeof(buffer); i++)
  {
    if (buffer[i] == ' ')
    {
      buffer[i] = '\0';
      printf("hello");
      tokens[index] = buffer + i + 1;
      index++;
    }
  }

  printf("\n\n--- tokens array ---\n");
  for (int i = 0; i <= index; i++)
  {
    printf("%d:%s\n", i, tokens[i]);
  }


  return 0;
}
