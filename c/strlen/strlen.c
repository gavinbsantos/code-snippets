#include <stdio.h>
#include <string.h>

int main()
{
    char string[] = "Hello";
    // Note that strlen does not include the '\0' at the end of the string
    printf("strlen(string): %i\n", strlen(string));
    return 0;
}
