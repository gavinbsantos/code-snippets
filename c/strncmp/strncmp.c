#include <stdio.h>
#include <string.h>

int main()
{
    char strings[][5] = {"RTE2", "NE21", "RT34", "NA2E", "NAOJ"};

    printf("\nPrinting all strings\n");
    for (int i = 0; i < 5; i++)
    {
        printf("%i: %s\n", i, strings[i]);
    }

    // Note that the "xxx"s don't matter here because of the third parameter of
    // strnmpy().
    printf("\nSearching for \"RTxx\"\n");
    for (int i = 0; i < 5; i++)
    {
        if (strncmp(strings[i], "RTxx", 2) == 0)
        {
            printf("%i: %s\n", i, strings[i]);
        }
    }

    printf("\nSearching for \"Nxxx\"\n");
    for (int i = 0; i < 5; i++)
    {
        if (strncmp(strings[i], "NAgi", 1) == 0)
        {
            printf("%i: %s\n", i, strings[i]);
        }
    }

    printf("\nPrinting all strings\n");
    for (int i = 0; i < 5; i++)
    {
        printf("%i: %s\n", i, strings[i]);
    }

    return 0;
}
