#include <stdio.h>
#include <string.h>

int main()
{
    char string[] = "Hello World";
    char temp[5];
    strncpy(temp, string, 4);

    printf("temp == 'Hell'?: %i\n", strcmp(temp, "Hell"));
    temp[4] = '\0';   // Needed otherwise the program will go way beyond
                      // while using functions like printf

    // interestingly when using strncpy it does not put a '\0' at the end so in
    // this case if you try to compare temp with "Hell" it won't be successful
    // because the '\0' is missing

    printf("temp == 'Hell'?: %i\n", strcmp(temp, "Hell"));
    printf("0-3: %s\n", temp);

    return 0;
}
