#include <stdio.h>
#include <string.h>

int main ()
{
    // Note that this function does not play nice when trying to tokenize a 
    // token
    char string[256] = "The thing,The Thought,The Thang";

    printf("--- Test 1 ---");

    printf("the inital value in \"string\": %s\n", string);
    char * token = strtok(string, ",");
    printf("the inital value in \"string\" after strtok: %s\n", string);


    while (token != NULL)
    {
        // note that strtok modifies the original string
        printf("token: %s\n", token);
        token = strtok(NULL, ",");
    }

    printf("the final value in \"string\": %s\n", string);

    return 0;
}
