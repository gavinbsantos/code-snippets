#include <iostream>
#include <vector>
#include <algorithm>

class Num
{
public:
  int num;

  Num(int n)
  {
    num = n;
  }

  bool operator==(const Num n)
  {
    return this->num == n.num;
  }

};


void printVector(std::vector<Num> vector)
{
  for (long unsigned int i = 0; i < vector.size(); i++)
    {
      std::cout << i << ": " << vector[i].num << std::endl;
    }
}


int main()
{
  std::vector<Num> numbers;

  // add elements to numbers
  std::cout << "Initialsing vector" << std::endl;
  for (int i = 0; i < 10; i++)
  {
    numbers.push_back(Num(i));
  }

  printVector(numbers);

  std::cout << "\nremoving an element" << std::endl;
  numbers.erase(std::remove(numbers.begin(), numbers.end(), Num(7)));
  printVector(numbers);

  std::cout << "\nremoving another element" << std::endl;
  numbers.erase(std::remove(numbers.begin(), numbers.end(), Num(2)));
  printVector(numbers);

  return 0;
}
