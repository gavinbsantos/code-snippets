class Enums {
    public enum Rank {
        ACE('A'), TWO('2'), THREE('3'), FOUR('4'), FIVE('5'),
        SIX('6'), SEVEN('7'), EIGHT('8'), NINE('9'), TEN('T'),
        JACK('J'), QUEEN('Q'), KING('K');

        private final char symbol;

        Rank(char s) { symbol = s; }

        public char getSymbol() { return symbol; }

        @Override
        public String toString() {
            return name().charAt(0) + name().substring(1).toLowerCase();
        }
    }

    public static void main(String[] args) {
        Rank rank = Rank.KING;

        System.out.println(rank.ordinal() + 1);
    }
}
