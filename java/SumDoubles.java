// Sums up doubles as many doubles as the user wants
// Uses a "try with resources" block and an end of file thingy

import java.util.Scanner;

class SumDoubles
{
    public static void main(String[] args)
    {
        double sum = 0.0;

        System.out.println("Enter numbers to be added, or Ctrl+D to quit");

        try (Scanner stdin = new Scanner(System.in))
        {
            while (stdin.hasNextDouble())
            {
                sum += stdin.nextDouble();
            }
        }

        System.out.printf("Sum = %f\n", sum);
    }
}
