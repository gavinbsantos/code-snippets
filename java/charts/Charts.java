import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.chart.XYChart.Data;
import javafx.stage.Stage;

public class Charts extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start (Stage primaryStage) {
        primaryStage.setTitle("Something interesting");
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("Numbers");
        yAxis.setLabel("Not Numbers");
        xAxis.setForceZeroInRange(false);

        final LineChart<Number, Number> lineChart = 
                new LineChart<Number, Number>(xAxis, yAxis);

        lineChart.setTitle("idk, man");
        lineChart.setCreateSymbols(false);
        Series series = new Series();
        series.setName("A line");
        for (int i = 50; i <= 100; i++) {
            series.getData().add(new Data(i, i + 2));
        }

        Scene scene = new Scene(lineChart, 800, 600);
        lineChart.getData().add(series);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
