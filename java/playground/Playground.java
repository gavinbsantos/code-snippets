import java.util.Thread;
import java.net.Socket;

class Playground extends Thread {
    private int numThreads = 10;
    private int port = 80;
    private String hostname = "Thingy.com";

    public void run() {
        Socket s = new Socket(hostname, port);
    }

    public static void main(String[] args) {
        for (int i = 0; i < numThreads; i++) {
            Playground t = new Playground();
            t.start();
        }
    }
}
