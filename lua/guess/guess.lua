math.randomseed(os.time())
running = true

while running do
  -- Get range
  range = 10

  -- Play the game
  compNum = math.random(range)
  humNum = nil
  guesses = 0
  while not (humNum == compNum) do
    io.write("Guess the number: ")
    humNum = tonumber(io.read())
    guesses = guesses + 1
    if humNum > compNum then
      print("Guess lower")
    elseif humNum < compNum then
      print("Guess higher")
    end
  end

  print("You guessed right!")
  if guesses == 1 then
    print("It took you 1 guess!")
  else
    print(string.format("It took you %d guesses.", guesses))
  end

  -- ask to exit

  running = false
end
