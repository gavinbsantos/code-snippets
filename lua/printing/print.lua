-- Both print() and io.write() are different
io.write("(io.write) Enter something: ")
io.read()

print("(print) enter something 2: ")
io.read()

-- io.write()s are more flexible
file = io.open("test.txt", "w")

io.write("Enter something 3: ")
io.output(file)
io.write("Enter something 4: ")
io.write(io.read())
io.close(file)

io.output(stdout)
print("This works!")
