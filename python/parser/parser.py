class Node:
    def __init__(self):
        self.id = 0
        self.lat = 0.0
        self.lon = 0.0


class Link:
    def __init__(self):
        self.id = 0
        self.node1 = 0
        self.node2 = 0
        self.length = 0


nodes = []
links = []

# minLat, minLon, maxLat, maxLon
bounds = [0, 0, 0, 0]


def parse(string):
    parts = string.split(" ")
    if parts[0] == "<node":  # Parse Node
        tmp = Node()
        for token in parts[1:-1]:
            if token[:2] == "id":
                tmp.id = int(token[3:])
            elif token[:3] == "lat":
                tmp.lat = float(token[4:])
            elif token[:3] == "lon":
                tmp.lon = float(token[4:])
        nodes.append(tmp)

    elif parts[0] == "<link":  # Parse Link
        tmp = Link()
        for token in parts[1:-1]:
            if token[:4] == "node":
                if tmp.node1:
                    tmp.node2 = int(token[5:])
                else:
                    tmp.node1 = int(token[5:])
            elif token[:2] == "id":
                tmp.id = int(token[3:])
            elif token[:6] == "length":
                tmp.length = float(token[7:])
        links.append(tmp)

    elif parts[0] == "<bounding":  # Parse Bounding
        for token in parts[1:-1]:
            if token[:6] == "maxLat":
                bounds[2] = float(token[7:])
            elif token[:6] == "maxLon":
                bounds[3] = float(token[7:])
            elif token[:6] == "minLon":
                bounds[1] = float(token[7:])
            elif token[:6] == "minLat":
                bounds[0] = float(token[7:])


with open("test.txt", "r") as file:
    content = file.readlines()
    for i in content:
        parse(i[:-1])


print("\n--- Boundries ---")
print("maxLat: " + str(bounds[2]))
print("maxLon: " + str(bounds[3]))
print("minLat: " + str(bounds[0]))
print("minLon: " + str(bounds[1]))
print("------")

print("\n\n--- Nodes ---")
for i in range(len(nodes)):
    print("\n--- " + str(i) + " ---")
    print("id: " + str(nodes[i].id))
    print("lat: " + str(nodes[i].lat))
    print("lon: " + str(nodes[i].lon))

print("------")

print("\n\n--- Links ---")
for i in range(len(links)):
    print("\n--- " + str(i) + " ---")
    print("id: " + str(links[i].id))
    print("node1: " + str(links[i].node1))
    print("node2: " + str(links[i].node2))
    print("length: " + str(links[i].length))

print("------")
