def num_pos_in_layout(n, layout):
    for i, j in enumerate(layout):
        for k, l, in enumerate(j):
            print("k: ", k, ", l: ", l)
            if l == n:
                return (i, k)

i = [ ['a', 'b', 'c'],
      ['d', 'e', 'f'],
      ['g', 'h', 'i'] ]

print(num_pos_in_layout('e', i))
